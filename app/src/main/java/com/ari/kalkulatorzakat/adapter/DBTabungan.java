package com.ari.kalkulatorzakat.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ari.kalkulatorzakat.model.ModelNisabEmasPerak;
import com.ari.kalkulatorzakat.model.ModelTabungan;
import com.ari.kalkulatorzakat.util.SharedPref;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ARI on 21/09/2017.
 */
public class DBTabungan extends SQLiteAssetHelper {
    private Context c;

    public DBTabungan(Context context){
        super(context, "zakat.db", null,1);
        this.c = context;

    }
    public void pushZakat(ModelTabungan modelTabungan){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("jumlah_tabungan",modelTabungan.getJumlahTabungan());
        contentValues.put("harga_pergram",modelTabungan.getHargaZakat1());
        contentValues.put("jenis_nisabtb",modelTabungan.getJenisNisab());
        contentValues.put("tgl_zakat",modelTabungan.getTglZakat());
        contentValues.put("tgl_hijri",modelTabungan.getTglHijri());
        db.insertOrThrow("tabungan","",contentValues);
    }
    public List<ModelTabungan> getZakat(){
        List<ModelTabungan> models = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select from * tabungan",null);
        if (cursor.getCount()==1){
            cursor.moveToFirst();
            ModelTabungan model = new ModelTabungan();
            model.setIdTabungan(cursor.getInt(cursor.getColumnIndex("id_tabungan")));
            model.setFlag(cursor.getInt(cursor.getColumnIndex("flag")));
            model.setJumlahTabungan(cursor.getDouble(cursor.getColumnIndex("jumlah_tabungan")));
            model.setHargaZakat1(cursor.getDouble(cursor.getColumnIndex("harga_pergram")));
            model.setJenisNisab(cursor.getString(cursor.getColumnIndex("jenis_nisabtb")));
            model.setTglHijri(cursor.getString(cursor.getColumnIndex("tgl_hijri")));
            model.setTglZakat(cursor.getString(cursor.getColumnIndex("tgl_zakat")));
            model.setKonversi(cursor.getDouble(cursor.getColumnIndex("konversi")));
            models.add(model);
        }else{
            while (cursor.moveToNext()){
                ModelTabungan model = new ModelTabungan();
                model.setIdTabungan(cursor.getInt(cursor.getColumnIndex("id_tabungan")));
                model.setFlag(cursor.getInt(cursor.getColumnIndex("flag")));
                model.setJumlahTabungan(cursor.getDouble(cursor.getColumnIndex("jumlah_tabungan")));
                model.setHargaZakat1(cursor.getDouble(cursor.getColumnIndex("harga_pergram")));
                model.setJenisNisab(cursor.getString(cursor.getColumnIndex("jenis_nisabtb")));
                model.setTglHijri(cursor.getString(cursor.getColumnIndex("tgl_hijri")));
                model.setTglZakat(cursor.getString(cursor.getColumnIndex("tgl_zakat")));
                model.setKonversi(cursor.getDouble(cursor.getColumnIndex("konversi")));
                models.add(model);
            }
        }
        cursor.close();
        return models;
    }
    public  double getTotalEmas(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select sum(jumlah_zakat) as total from emas where flag = "+";",null );
        cursor.moveToFirst();
        return cursor.getInt(cursor.getColumnIndex("total"));
    }
    public void pushNisabEmasPerak(ModelNisabEmasPerak modelNisabEmasPerak){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("jenis",modelNisabEmasPerak.getJenis());
        contentValues.put("tanggal_mulaihaul",modelNisabEmasPerak.getTanggal_mulaihaul());
        contentValues.put("tanggal_akhirhaul",modelNisabEmasPerak.getTanggal_akhirhaul());
        contentValues.put("jumlah_harta",modelNisabEmasPerak.getJumlah_harta());
        contentValues.put("jumlah_nisab",modelNisabEmasPerak.getJumlah_nisab());
        contentValues.put("konversi",modelNisabEmasPerak.getKonversi());
        contentValues.put("tanggal_hijriawal",modelNisabEmasPerak.getTanggal_hijriawal());
        contentValues.put("tanggal_hijriahir",modelNisabEmasPerak.getTanggal_hijriahir());
        db.insertOrThrow("nisabhaul","",contentValues);
    }
    public boolean isHaul(String jenis){
        SQLiteDatabase db = this.getReadableDatabase();
        String query =  "";
        if (jenis.trim().equalsIgnoreCase("emas")){
            query = "select * from emas";
        }else{
            query = "select * from perak";
        }
        Cursor cursor = db.rawQuery(query,null);
        if (cursor.getCount()%11 == 0&& cursor.getCount()>11){
            return true;
        }else{
            return false;
        }
    }
    public int getCount(String jenis){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "";
        if (jenis.trim().equalsIgnoreCase("perak")){
            query = "select * from emas";
        }else{
            query = "select * from perak";
        }
        Cursor cursor = db.rawQuery(query,null);
        return cursor.getCount();
    }
    public void hapusItem(int idTabungan) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("DELETE FROM tani WHERE id_tabungan='" + idTabungan + "'");
        database.close();
    }
    public int editItem(int idTabungan, Double hargaZakat1, Double jumlahTabungan, String jenisNisab) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("harga_pergram", hargaZakat1);
        contentValues.put("jumlah_tabungan", jumlahTabungan);
        contentValues.put("jenis_nisabtb", jenisNisab);
        return database.update("tabungan", contentValues, "id_tabungan" + idTabungan, null);

    }


}
