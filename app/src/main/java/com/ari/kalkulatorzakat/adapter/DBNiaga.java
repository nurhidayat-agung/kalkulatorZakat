package com.ari.kalkulatorzakat.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ari.kalkulatorzakat.model.ModelNisabEmasPerak;
import com.ari.kalkulatorzakat.model.ModelPerniagaan;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ARI on 08/10/2017.
 */
public class DBNiaga extends SQLiteAssetHelper {
    private Context c;

    public DBNiaga(Context context){
        super(context,"zakat.db",null,1);
        this.c = context;
    }
    public void pushZakat(ModelPerniagaan modelPerniagaan){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("harga_pergram",modelPerniagaan.getHarga3());
        contentValues.put("modal",modelPerniagaan.getModal());
        contentValues.put("untung",modelPerniagaan.getUntung());
        contentValues.put("piutang",modelPerniagaan.getPiutang());
        contentValues.put("hutang",modelPerniagaan.getHutang());
        contentValues.put("tgl_zakat",modelPerniagaan.getTglZakat());
        contentValues.put("tgl_hijri",modelPerniagaan.getTglHijri());
        contentValues.put("jenis_nisabniaga",modelPerniagaan.getJenisNisab());
        db.insertOrThrow("perniagaan","",contentValues);

    }
    public List<ModelPerniagaan> getZakat() {
        List<ModelPerniagaan> models = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from perniagaan", null);
        if (cursor.getCount() == 1) {
            cursor.moveToFirst();
            ModelPerniagaan model = new ModelPerniagaan();
            model.setIdNiaga(cursor.getInt(cursor.getColumnIndex("id_niaga")));
            model.setFlag(cursor.getInt(cursor.getColumnIndex("flag")));
            model.setHarga3(cursor.getDouble(cursor.getColumnIndex("harga_pergram")));
            model.setModal(cursor.getDouble(cursor.getColumnIndex("modal")));
            model.setUntung(cursor.getDouble(cursor.getColumnIndex("untung")));
            model.setPiutang(cursor.getDouble(cursor.getColumnIndex("piutang")));
            model.setHutang(cursor.getDouble(cursor.getColumnIndex("hutang")));
            model.setTglZakat(cursor.getString(cursor.getColumnIndex("tgl_zakat")));
            model.setTglHijri(cursor.getString(cursor.getColumnIndex("tgl_hijri")));
            model.setJenisNisab(cursor.getString(cursor.getColumnIndex("jenis_nisabniaga")));
            model.setKonversi(cursor.getDouble(cursor.getColumnIndex("konversi")));
            models.add(model);
        } else {
            while (cursor.moveToNext()) {
                ModelPerniagaan model = new ModelPerniagaan();
                model.setIdNiaga(cursor.getInt(cursor.getColumnIndex("id_niaga")));
                model.setFlag(cursor.getInt(cursor.getColumnIndex("flag")));
                model.setHarga3(cursor.getDouble(cursor.getColumnIndex("harga_pergram")));
                model.setModal(cursor.getDouble(cursor.getColumnIndex("modal")));
                model.setUntung(cursor.getDouble(cursor.getColumnIndex("untung")));
                model.setPiutang(cursor.getDouble(cursor.getColumnIndex("piutang")));
                model.setHutang(cursor.getDouble(cursor.getColumnIndex("hutang")));
                model.setTglZakat(cursor.getString(cursor.getColumnIndex("tgl_zakat")));
                model.setTglHijri(cursor.getString(cursor.getColumnIndex("tgl_hijri")));
                model.setJenisNisab(cursor.getString(cursor.getColumnIndex("jenis_nisabniaga")));
                model.setKonversi(cursor.getDouble(cursor.getColumnIndex("konversi")));
                models.add(model);
            }
        }
        cursor.close();
        return models;
    }
    public double getTotalEmas(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select sum(jumlah_zakat) as total from emas where flag = "+";",null);
        cursor.moveToFirst();
        return cursor.getInt(cursor.getColumnIndex("total"));
    }
    public void  pustNisabEmasPerak(ModelNisabEmasPerak modelNisabEmasPerak){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("jenis",modelNisabEmasPerak.getJenis());
        contentValues.put("tanggal_mulaihaul",modelNisabEmasPerak.getTanggal_mulaihaul());
        contentValues.put("tanggal_akhirhaul",modelNisabEmasPerak.getTanggal_akhirhaul());
        contentValues.put("jumlah_harta",modelNisabEmasPerak.getJumlah_harta());
        contentValues.put("jumlah_nisab",modelNisabEmasPerak.getJumlah_nisab());
        contentValues.put("konversi",modelNisabEmasPerak.getKonversi());
        contentValues.put("tanggal_hijriawal",modelNisabEmasPerak.getTanggal_hijriawal());
        contentValues.put("tanggal_hijriahir",modelNisabEmasPerak.getTanggal_hijriahir());
        db.insertOrThrow("nisabhaul","",contentValues);
    }
    public  int getLastID (String jenis){
        int lastid = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        String sql ="select id_niaga from "+jenis+"order by id_niaga desc limit 1";
        Cursor cursor = db.rawQuery(sql,null);
        if (cursor.getCount()==1){
            cursor.moveToFirst();
            lastid = cursor.getInt(cursor.getColumnIndex("id_niaga"));
        }
        return lastid;
    }

    public boolean isHaul(String jenis){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "";
        if (jenis.trim().equalsIgnoreCase("emas")){
            query = "select * from emas";
        }else {
            query = "select * from perak";
        }
        Cursor cursor = db.rawQuery(query,null);
        if (cursor.getCount()%11 == 0&& cursor.getCount()>=11){
            return  true;

        }else {
            return false;
        }
    }

    public int getCount(String jenis){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "";
        if (jenis.trim().equalsIgnoreCase("emas")){
            query = "select * from emas";
        }else {
            query = "select * from perak";
        }
        Cursor cursor = db.rawQuery(query,null);
        return cursor.getCount();
    }
    public void hapusItem(int idNiaga) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("DELETE FROM tani WHERE id_niaga='" + idNiaga + "'");
        database.close();
    }
    public int editItem(int idNiaga, Double harga3, Double modal, Double untung, Double hutang, Double piutang ,String jenisNisab) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("harga_pergram", harga3);
        contentValues.put("modal", modal);
        contentValues.put("untung", untung);
        contentValues.put("hutang", hutang);
        contentValues.put("piutang", piutang);
        contentValues.put("jenis_nisabniaga", jenisNisab);
        return database.update("perniagaan", contentValues, "id_niaga" + idNiaga, null);

    }

}
