package com.ari.kalkulatorzakat.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ari.kalkulatorzakat.R;
import com.ari.kalkulatorzakat.model.ModelEmasPerak;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ARI on 24/08/2017.
 */
public class RVEmasPerak extends RecyclerView.Adapter<RVEmasPerak.MyViewHolder>{
    private Context c;
    private List<ModelEmasPerak> models = new ArrayList<>();
    private DBEmasPerak dbEmasPerak;

    public RVEmasPerak(Context c, List<ModelEmasPerak> models, DBEmasPerak dbEmasPerak) {
        this.c = c;
        this.models = models;
        this.dbEmasPerak = dbEmasPerak;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_emasperak,parent,false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final ModelEmasPerak modelEmasPerak = models.get(position);
        NumberFormat emass = NumberFormat.getCurrencyInstance();
        holder.tvTgl.setText(modelEmasPerak.getTglZakat());
        holder.tvJumlah.setText(String.valueOf(emass.format(modelEmasPerak.getJumlahZakat())));
        holder.tvHarga.setText(String.valueOf(emass.format(modelEmasPerak.getHargaPergram())));
        holder.tvEmasPerak.setText(modelEmasPerak.getJenis());
        holder.tvTanggalHijri.setText(modelEmasPerak.getTglHijri());
        if (modelEmasPerak.getFlag()== 1){
            holder.llKonversi.setVisibility(View.VISIBLE);
            holder.tvKonversi.setText(modelEmasPerak.getKonversi()+"");
            Toast.makeText(c, "testlala", Toast.LENGTH_SHORT).show();
        }else{
            holder.llKonversi.setVisibility(View.GONE);
            Toast.makeText(c, "test2", Toast.LENGTH_SHORT).show();
        }
        holder.btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (modelEmasPerak.getJenis().toLowerCase().trim().equals("emas")){
                    dbEmasPerak.hapusItemEmas(modelEmasPerak.getIdZakat());
                }else{
                    dbEmasPerak.hapusItemPerak(modelEmasPerak.getIdZakat());
                }
                models.clear();
                models = dbEmasPerak.getZakat();
                notifyDataSetChanged();
            }
        });
        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return models.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTgl,tvEmasPerak,tvJumlah,tvHarga,tvTanggalHijri,tvKonversi;
        public LinearLayout llKonversi;
        public Button btnEdit,btnHapus;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvTgl = (TextView) itemView.findViewById(R.id.tv_tanggal);
            tvEmasPerak = (TextView) itemView.findViewById(R.id.tv_emasperak);
            tvJumlah = (TextView) itemView.findViewById(R.id.tv_jumlahzakat);
            tvHarga = (TextView) itemView.findViewById(R.id.tv_harga);
            tvTanggalHijri = (TextView) itemView.findViewById(R.id.tv_tanggalhijri);
            tvKonversi = (TextView) itemView.findViewById(R.id.tv_konversi);
            llKonversi = (LinearLayout) itemView.findViewById(R.id.ll_konversi);
            btnEdit = (Button) itemView.findViewById(R.id.edit_zakat);
            btnHapus = (Button) itemView.findViewById(R.id.hapus_zakat);
        }
    }
}
