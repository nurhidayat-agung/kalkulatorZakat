package com.ari.kalkulatorzakat.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ari.kalkulatorzakat.R;
import com.ari.kalkulatorzakat.model.ModelTabungan;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ARI on 06/10/2017.
 */
public class RVTabungan extends RecyclerView.Adapter<RVTabungan.TabunganViewHolder> {
    private Context c;
    private List<ModelTabungan> models = new ArrayList<>();

    public RVTabungan(Context c, List<ModelTabungan>models){
        this.c = c;
        this.models = models;
    }


    @Override
    public TabunganViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tabungan,parent,false);
        return new TabunganViewHolder(view);
    }


    @Override
    public void onBindViewHolder(TabunganViewHolder holder, int position) {
        final ModelTabungan modelTabungan = models.get(position);
        NumberFormat tabungann = NumberFormat.getCurrencyInstance();
        holder.tvJumlah.setText(String.valueOf(tabungann.format(modelTabungan.getJumlahTabungan())));
        holder.tvHarga1.setText(String.valueOf(tabungann.format(modelTabungan.getHargaZakat1())));
        holder.tvJenis.setText(modelTabungan.getJenisNisab());
        holder.tvTanggal.setText(modelTabungan.getTglZakat());
        holder.tvHijri.setText(modelTabungan.getTglHijri());

    }


    @Override
    public int getItemCount() {
        return models.size();
    }

    public class TabunganViewHolder extends RecyclerView.ViewHolder{
        public TextView tvTanggal, tvHijri, tvJenis, tvJumlah, tvHarga1,tvKonversi;
        public LinearLayout llKonversi;
        public Button btnEdit,btnHapus;
        public TabunganViewHolder(View itemView) {
            super(itemView);
            tvTanggal = (TextView) itemView.findViewById(R.id.tv_tanggal);
            tvHijri = (TextView) itemView.findViewById(R.id.tv_tanggalhijri);
            tvJenis = (TextView) itemView.findViewById(R.id.tv_nisab);
            tvJumlah = (TextView) itemView.findViewById(R.id.tv_jumlah);
            tvHarga1 = (TextView) itemView.findViewById(R.id.tv_harga1);
            tvKonversi = (TextView) itemView.findViewById(R.id.tv_konversi1);
            llKonversi = (LinearLayout) itemView.findViewById(R.id.ll_konversi1);
            btnEdit = (Button) itemView.findViewById(R.id.edit_zakat);
            btnHapus = (Button) itemView.findViewById(R.id.hapus_zakat);

        }
    }
}
