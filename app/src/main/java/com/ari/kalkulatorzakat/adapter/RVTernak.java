package com.ari.kalkulatorzakat.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.ari.kalkulatorzakat.R;
import com.ari.kalkulatorzakat.model.ModelTernak;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ARI on 06/10/2017.
 */
public class RVTernak extends RecyclerView.Adapter<RVTernak.TernakViewHolder> {
    private Context c;
    private List<ModelTernak> models = new ArrayList<>();

    public RVTernak(Context c, List<ModelTernak> models){
        this.c = c;
        this.models = models;
    }


    @Override
    public TernakViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_perternakkan,parent,false);
        return new TernakViewHolder(view);
    }


    @Override
    public void onBindViewHolder(TernakViewHolder holder, int position) {

        ModelTernak modelTernak = new ModelTernak();
        modelTernak = models.get(position);
        holder.tvTernak.setText(modelTernak.getJumlahTernak()+"");
        holder.tvHasil.setText(modelTernak.getHasil());
        holder.tvJenis.setText(modelTernak.getJenisTernak());
        holder.tvTanggal.setText(modelTernak.getTglZakat());
        holder.tvHijri.setText(modelTernak.getTglHijri());

    }


    @Override
    public int getItemCount() {
        return models.size();
    }

    public class TernakViewHolder extends RecyclerView.ViewHolder{
        public TextView tvTanggal, tvHijri, tvJenis, tvTernak,tvHasil;
        public Button btnEdit,btnHapus;
        public TernakViewHolder(View itemView) {
            super(itemView);
            tvTanggal = (TextView) itemView.findViewById(R.id.tv_tanggal);
            tvHijri = (TextView) itemView.findViewById(R.id.tv_tanggalhijri);
            tvJenis = (TextView) itemView.findViewById(R.id.tv_jenis);
            tvTernak = (TextView) itemView.findViewById(R.id.tv_ternak);
            tvHasil = (TextView) itemView.findViewById(R.id.tv_hasil);
            btnEdit = (Button) itemView.findViewById(R.id.edit_zakat);
            btnHapus = (Button) itemView.findViewById(R.id.hapus_zakat);
        }
    }
}
