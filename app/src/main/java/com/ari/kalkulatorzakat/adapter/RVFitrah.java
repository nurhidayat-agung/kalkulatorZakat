package com.ari.kalkulatorzakat.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ari.kalkulatorzakat.R;
import com.ari.kalkulatorzakat.model.ModelFitrah;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ARI on 06/10/2017.
 */
public class RVFitrah extends RecyclerView.Adapter<RVFitrah.FitrahViewHolder>{
    private Context c;
    private List<ModelFitrah> models = new ArrayList<>();
    private DBFitrah dbFitrah;
    private ModelFitrah modelFitrah = new ModelFitrah();

    public RVFitrah(Context c, List<ModelFitrah> models){
        this.c = c;
        this.models = models;
        this.dbFitrah = dbFitrah;
    }


    @Override
    public FitrahViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_fitrah,parent,false);
        return new FitrahViewHolder(view);
    }


    @Override
    public void onBindViewHolder(FitrahViewHolder holder, int position) {
        modelFitrah = models.get(position);
        NumberFormat fitrahh = NumberFormat.getCurrencyInstance();
        holder.tvJumlahOrang.setText(modelFitrah.getJumlahOrang()+"");
        holder.tvHasil3.setText(String.valueOf(fitrahh.format(modelFitrah.getHasil3())));
        holder.tvHarga5.setText(String.valueOf(fitrahh.format(modelFitrah.getHarga5())));
        holder.tvJenis.setText(modelFitrah.getJenisZakat());
        holder.tvTanggal.setText(modelFitrah.getTglZakat());
        holder.tvHijri.setText(modelFitrah.getTglHijri());
        holder.btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (modelFitrah.getJenisZakat().toLowerCase().trim().equals("berat")){
                    dbFitrah.hapusItem(modelFitrah.getIdFitrah());
                }else{
                    dbFitrah.hapusItem(modelFitrah.getIdFitrah());
                }
                models.clear();
                models = dbFitrah.getZakat();
                notifyDataSetChanged();
            }
        });

    }


    @Override
    public int getItemCount() {
        return models.size();
    }

    public class FitrahViewHolder extends RecyclerView.ViewHolder{
        public TextView tvTanggal, tvHijri, tvJenis, tvJumlahOrang, tvHasil3, tvHarga5;
        public Button btnHapus,btnEdit;
        public FitrahViewHolder(View itemView) {
            super(itemView);
            tvTanggal = (TextView) itemView.findViewById(R.id.tv_tanggal);
            tvHijri = (TextView) itemView.findViewById(R.id.tv_tanggalhijri);
            tvJenis = (TextView) itemView.findViewById(R.id.tv_jenis);
            tvHarga5 = (TextView) itemView.findViewById(R.id.tv_harga5);
            tvJumlahOrang = (TextView) itemView.findViewById(R.id.tv_wajib);
            tvHasil3 = (TextView) itemView.findViewById(R.id.tv_hasil3);
            btnEdit = (Button) itemView.findViewById(R.id.edit_zakat);
            btnHapus = (Button) itemView.findViewById(R.id.hapus_zakat);
        }
    }
}
