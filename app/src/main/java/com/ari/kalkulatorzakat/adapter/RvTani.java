package com.ari.kalkulatorzakat.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ari.kalkulatorzakat.R;
import com.ari.kalkulatorzakat.model.ModelPertanian;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ARI on 03/10/2017.
 */
public class RvTani extends RecyclerView.Adapter<RvTani.TaniViewHolder> {
    private Context c;
    private List<ModelPertanian> models = new ArrayList<>();

    public RvTani(Context c, List<ModelPertanian> models) {
        this.c = c;
        this.models = models;
    }

    @Override
    public TaniViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pertanian,parent,false);
        return new TaniViewHolder(view);
    }


    @Override
    public void onBindViewHolder(TaniViewHolder holder, int position) {

        ModelPertanian modelPertanian = new ModelPertanian();
        modelPertanian = models.get(position);
        NumberFormat tanii = NumberFormat.getCurrencyInstance();
        holder.tvHasil1.setText(String.valueOf(tanii.format(modelPertanian.getHasil1())));
        holder.tvHarga6.setText(String.valueOf(tanii.format(modelPertanian.getHargaJenis())));
        holder.tvJumlah.setText(String.valueOf(tanii.format(modelPertanian.getJumlahPanen())));
        holder.tvJenis1.setText(modelPertanian.getJenisPanen());
        holder.tvJenis.setText(modelPertanian.getJenis());
        holder.tvHijri.setText(modelPertanian.getTglHijri());
        holder.tvTanggal.setText(modelPertanian.getTglZakat());
    }


    @Override
    public int getItemCount() {
        return models.size();
    }

    public class TaniViewHolder extends RecyclerView.ViewHolder{
        public TextView tvTanggal,tvHijri,tvJenis,tvJenis1,tvJumlah,tvHarga6,tvHasil1,tvKonversi4;
        public Button btnEdit, btnHapus;
        public TaniViewHolder(View itemView) {
            super(itemView);
            tvTanggal = (TextView) itemView.findViewById(R.id.tv_tanggal);
            tvHijri = (TextView) itemView.findViewById(R.id.tv_tanggalhijri);
            tvJenis1 = (TextView) itemView.findViewById(R.id.tv_jenis1);
            tvJenis = (TextView) itemView.findViewById(R.id.tv_jenis);
            tvJumlah = (TextView) itemView.findViewById(R.id.tv_jumlah);
            tvHarga6 = (TextView) itemView.findViewById(R.id.tv_harga6);
            tvHasil1 = (TextView) itemView.findViewById(R.id.tv_hasil1);
            tvKonversi4 = (TextView) itemView.findViewById(R.id.tv_konversi4);
            btnEdit = (Button) itemView.findViewById(R.id.edit_zakat);
            btnHapus = (Button) itemView.findViewById(R.id.hapus_zakat);
        }
    }
}
