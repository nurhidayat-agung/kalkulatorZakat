package com.ari.kalkulatorzakat.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.ari.kalkulatorzakat.model.ModelTernak;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ARI on 12/10/2017.
 */
public class DBTernak extends SQLiteAssetHelper {
    private Context c;

    public DBTernak(Context context){
        super(context,"zakat.db", null, 1);
        this.c = context;
    }
    public void pushZakat(ModelTernak modelTernak){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("jumlah_ternak",modelTernak.getJumlahTernak());
        contentValues.put("hasil",modelTernak.getHasil());
        contentValues.put("jenis",modelTernak.getJenisTernak());
        contentValues.put("tgl_zakat",modelTernak.getTglZakat());
        contentValues.put("tgl_hijri",modelTernak.getTglHijri());
        db.insertOrThrow("ternak","",contentValues);
        Toast.makeText(c, "masuk kok", Toast.LENGTH_SHORT).show();
    }
    public List<ModelTernak> getZakat() {
        List<ModelTernak> models = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from ternak", null);
        if (cursor.getCount() == 1) {
            cursor.moveToFirst();
            ModelTernak model = new ModelTernak();
            model.setIdZakat(cursor.getInt(cursor.getColumnIndex("id_ternak")));
            model.setTglZakat(cursor.getString(cursor.getColumnIndex("tgl_zakat")));
            model.setTglHijri(cursor.getString(cursor.getColumnIndex("tgl_hijri")));
            model.setJumlahTernak(cursor.getDouble(cursor.getColumnIndex("jumlah_ternak")));
            model.setJenisTernak(cursor.getString(cursor.getColumnIndex("jenis")));
            model.setHasil(cursor.getString(cursor.getColumnIndex("hasil")));
            models.add(model);
        } else {
            while (cursor.moveToNext()) {
                ModelTernak model = new ModelTernak();
                model.setIdZakat(cursor.getInt(cursor.getColumnIndex("id_ternak")));
                model.setTglZakat(cursor.getString(cursor.getColumnIndex("tgl_zakat")));
                model.setTglHijri(cursor.getString(cursor.getColumnIndex("tgl_hijri")));
                model.setJumlahTernak(cursor.getDouble(cursor.getColumnIndex("jumlah_ternak")));
                model.setJenisTernak(cursor.getString(cursor.getColumnIndex("jenis")));
                model.setHasil(cursor.getString(cursor.getColumnIndex("hasil")));
                models.add(model);
            }
        }
            cursor.close();
        Toast.makeText(c, "jumlah : "+ models.size(), Toast.LENGTH_SHORT).show();
            return models;
        }
    public void hapusItem(int idZakat) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("DELETE FROM tani WHERE id_ternak='" + idZakat + "'");
        database.close();
    }

    public int editItem(int idZakat, Double jumlahTernak, String JenisTernak) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("jumlah_ternak", jumlahTernak);
        contentValues.put("jenis", JenisTernak);
        return database.update("ternak", contentValues, "id_ternak" + idZakat, null);

    }
    }


