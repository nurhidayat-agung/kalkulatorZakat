package com.ari.kalkulatorzakat.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.ari.kalkulatorzakat.model.ModelPertanian;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ARI on 03/10/2017.
 */
public class DBTani extends SQLiteAssetHelper {
    private  Context c;

    public DBTani(Context context) {
        super(context,"zakat.db", null, 1);
        this.c = context;
    }

    public void pushZakat(ModelPertanian modelPertanian){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("harga_jenis",modelPertanian.getHargaJenis());
        contentValues.put("hasil",modelPertanian.getHasil1());
        contentValues.put("jumlah_panen",modelPertanian.getJumlahPanen());
        contentValues.put("konversi",modelPertanian.getKonversi4());
        contentValues.put("jenis_panen",modelPertanian.getJenisPanen());
        contentValues.put("jenis",modelPertanian.getJenis());
        contentValues.put("tgl_zakat",modelPertanian.getTglZakat());
        contentValues.put("tgl_hijri",modelPertanian.getTglHijri());
        db.insertOrThrow("tani","",contentValues);
        Toast.makeText(c, "masuk kok", Toast.LENGTH_SHORT).show();
    }

    public List<ModelPertanian> getZakat() {
        List<ModelPertanian> models = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from tani", null);
        if (cursor.getCount() == 1) {
            cursor.moveToFirst();
            ModelPertanian model = new ModelPertanian();
            model.setIdTani(cursor.getInt(cursor.getColumnIndex("id_tani")));
            model.setTglZakat(cursor.getString(cursor.getColumnIndex("tgl_zakat")));
            model.setTglHijri(cursor.getString(cursor.getColumnIndex("tgl_hijri")));
            model.setJenis(cursor.getString(cursor.getColumnIndex("jenis")));
            model.setJenisPanen(cursor.getString(cursor.getColumnIndex("jenis_panen")));
            model.setJumlahPanen(cursor.getDouble(cursor.getColumnIndex("jumlah_panen")));
            model.setHasil1(cursor.getDouble(cursor.getColumnIndex("hasil")));
            model.setHargaJenis(cursor.getDouble(cursor.getColumnIndex("harga_jenis")));
            model.setKonversi4(cursor.getDouble(cursor.getColumnIndex("konversi")));
            models.add(model);
        } else {
            while (cursor.moveToNext()) {
                ModelPertanian model = new ModelPertanian();
                model.setIdTani(cursor.getInt(cursor.getColumnIndex("id_tani")));
                model.setTglZakat(cursor.getString(cursor.getColumnIndex("tgl_zakat")));
                model.setTglHijri(cursor.getString(cursor.getColumnIndex("tgl_hijri")));
                model.setJenis(cursor.getString(cursor.getColumnIndex("jenis")));
                model.setJenisPanen(cursor.getString(cursor.getColumnIndex("jenis_panen")));
                model.setJumlahPanen(cursor.getDouble(cursor.getColumnIndex("jumlah_panen")));
                model.setHasil1(cursor.getDouble(cursor.getColumnIndex("hasil")));
                model.setHargaJenis(cursor.getDouble(cursor.getColumnIndex("harga_jenis")));
                model.setKonversi4(cursor.getDouble(cursor.getColumnIndex("konversi")));
                models.add(model);
            }
        }


            cursor.close();
            Toast.makeText(c, "jumlah : " + models.size(), Toast.LENGTH_SHORT).show();
            return models;
        }

    public void hapusItem(int idTani) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("DELETE FROM tani WHERE id_tani='" + idTani + "'");
        database.close();
    }

    public int editItem(int idTani, Double hargaJenis, Double jumlahPanen, String JenisPanen, String Jenis) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("harga_jenis", hargaJenis);
        contentValues.put("jumlah_panen", jumlahPanen);
        contentValues.put("jenis_panen", JenisPanen);
        contentValues.put("jenis", Jenis);
        return database.update("tani", contentValues, "id_tani" + idTani, null);

    }
}


