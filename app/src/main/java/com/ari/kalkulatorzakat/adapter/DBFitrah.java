package com.ari.kalkulatorzakat.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.ari.kalkulatorzakat.model.ModelFitrah;
import com.ari.kalkulatorzakat.model.ModelPerniagaan;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ARI on 09/10/2017.
 */
public class DBFitrah extends SQLiteAssetHelper {
    private Context c;

    public DBFitrah(Context context){
        super(context,"zakat.db",null,1);
        this.c = context;
    }
    public void pushZakat (ModelFitrah modelFitrah){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("jumlah_orang",modelFitrah.getJumlahOrang());
        contentValues.put("hasil",modelFitrah.getHasil3());
        contentValues.put("harga",modelFitrah.getHarga5());
        contentValues.put("jenis",modelFitrah.getJenisZakat());
        contentValues.put("tgl_zakat",modelFitrah.getTglZakat());
        contentValues.put("tgl_hijri",modelFitrah.getTglHijri());
        db.insertOrThrow("fitrah","",contentValues);
    }
    public List<ModelFitrah> getZakat(){
        List<ModelFitrah> models = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from fitrah",null);
        if (cursor.getCount() == 1){
            cursor.moveToFirst();
            ModelFitrah model = new ModelFitrah();
            model.setIdFitrah(cursor.getInt(cursor.getColumnIndex("id_fitrah")));
            model.setJumlahOrang(cursor.getDouble(cursor.getColumnIndex("jumlah_orang")));
            model.setHasil3(cursor.getDouble(cursor.getColumnIndex("hasil")));
            model.setHarga5(cursor.getDouble(cursor.getColumnIndex("harga")));
            model.setJenisZakat(cursor.getString(cursor.getColumnIndex("jenis")));
            model.setTglZakat(cursor.getString(cursor.getColumnIndex("tgl_zakat")));
            model.setTglHijri(cursor.getString(cursor.getColumnIndex("tgl_hijri")));
            models.add(model);
        }else{
            while (cursor.moveToNext()){
                ModelFitrah model = new ModelFitrah();
                model.setIdFitrah(cursor.getInt(cursor.getColumnIndex("id_fitrah")));
                model.setJumlahOrang(cursor.getDouble(cursor.getColumnIndex("jumlah_orang")));
                model.setHasil3(cursor.getDouble(cursor.getColumnIndex("hasil")));
                model.setHarga5(cursor.getDouble(cursor.getColumnIndex("harga")));
                model.setJenisZakat(cursor.getString(cursor.getColumnIndex("jenis")));
                model.setTglZakat(cursor.getString(cursor.getColumnIndex("tgl_zakat")));
                model.setTglHijri(cursor.getString(cursor.getColumnIndex("tgl_hijri")));
                models.add(model);

            }
        }
        cursor.close();
        Toast.makeText(c, "jumlah: "+ models.size(), Toast.LENGTH_SHORT).show();
        return models;
    }
    public void hapusItem(int idFitrah) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("DELETE FROM tani WHERE id_fitrah='" + idFitrah + "'");
        database.close();
    }

    public int editItem(int idFitrah, Double jumlahOrang, Double harga5) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("jumlah_orang", jumlahOrang);
        contentValues.put("harga", harga5);
        return database.update("fitrah", contentValues, "id_fitrah" + idFitrah, null);

    }


}
