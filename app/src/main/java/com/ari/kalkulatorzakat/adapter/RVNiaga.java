package com.ari.kalkulatorzakat.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ari.kalkulatorzakat.R;
import com.ari.kalkulatorzakat.model.ModelFitrah;
import com.ari.kalkulatorzakat.model.ModelPerniagaan;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ARI on 26/09/2017.
 */
public class RVNiaga extends RecyclerView.Adapter<RVNiaga.NiagaViewHolder> {
    private Context c;
    private List<ModelPerniagaan> models = new ArrayList<>();
    private DBNiaga dbNiaga;
    private ModelPerniagaan modelPerniagaan = new ModelPerniagaan();

    public RVNiaga(Context c, List<ModelPerniagaan> models, DBNiaga dbNiaga) {
        this.c = c;
        this.models = models;
        this.dbNiaga = dbNiaga;
    }

    @Override
    public NiagaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_perniagaan,parent,false);
        return new NiagaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NiagaViewHolder holder, int position) {

        modelPerniagaan = models.get(position);
        NumberFormat niagaa = NumberFormat.getCurrencyInstance();
        holder.tvTanggal.setText(modelPerniagaan.getTglZakat());
        holder.tvHijri.setText(modelPerniagaan.getTglHijri());
        holder.tvHarga3.setText(String.valueOf(niagaa.format(modelPerniagaan.getHarga3())));
        holder.tvModal.setText(String.valueOf(niagaa.format(modelPerniagaan.getModal())));
        holder.tvUntung.setText(String.valueOf(niagaa.format(modelPerniagaan.getUntung())));
        holder.tvPiutang.setText(String.valueOf(niagaa.format(modelPerniagaan.getPiutang())));
        holder.tvHutang.setText(String.valueOf(niagaa.format(modelPerniagaan.getHutang())));
        holder.tvjenisNisab.setText(modelPerniagaan.getJenisNisab());
        if (modelPerniagaan.getFlag()==1){
            holder.llKonversi2.setVisibility(View.VISIBLE);
            holder.tvKonversi2.setText(modelPerniagaan.getKonversi()+"");
        }else{
            holder.llKonversi2.setVisibility(View.GONE);
        }
        holder.btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (modelPerniagaan.getJenisNisab().toLowerCase().trim().equals("emas")){
                    
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return 10;
    }
    public class NiagaViewHolder extends RecyclerView.ViewHolder{
        public TextView tvTanggal,tvHijri,tvjenisNisab,tvModal,tvUntung,tvPiutang,tvHutang,tvHarga3,tvKonversi2;
        public LinearLayout llKonversi2;
        public Button btnEdit,btnHapus;
        public NiagaViewHolder(View itemView) {
            super(itemView);
            tvTanggal = (TextView) itemView.findViewById(R.id.tv_tanggal);
            tvHijri = (TextView) itemView.findViewById(R.id.tv_tanggalhijri);
            tvjenisNisab = (TextView) itemView.findViewById(R.id.tv_nisab);
            tvHarga3 = (TextView) itemView.findViewById(R.id.tv_harga3);
            tvModal = (TextView) itemView.findViewById(R.id.tv_modal);
            tvUntung = (TextView) itemView.findViewById(R.id.tv_untung);
            tvPiutang = (TextView) itemView.findViewById(R.id.tv_piutang);
            tvHutang = (TextView) itemView.findViewById(R.id.tv_hutang);
            tvKonversi2 = (TextView) itemView.findViewById(R.id.tv_konversi2);
            llKonversi2 = (LinearLayout) itemView.findViewById(R.id.ll_konversi2);
            btnEdit = (Button) itemView.findViewById(R.id.edit_zakat);
            btnHapus = (Button) itemView.findViewById(R.id.hapus_zakat);
        }
    }
}
