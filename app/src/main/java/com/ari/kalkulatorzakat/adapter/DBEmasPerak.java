package com.ari.kalkulatorzakat.adapter;

import android.app.DownloadManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ari.kalkulatorzakat.model.ModelEmasPerak;
import com.ari.kalkulatorzakat.model.ModelNisabEmasPerak;
import com.ari.kalkulatorzakat.util.SharedPref;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ARI on 22/08/2017.
 */
public class DBEmasPerak extends SQLiteAssetHelper {
    private Context c;
    private SharedPref sharedPref;

    public DBEmasPerak(Context context) {
        super(context, "zakat.db", null, 1);
        this.c = context;
        sharedPref = new SharedPref(context);
    }

    public void pushZakat(ModelEmasPerak modelEmasPerak,boolean flag){
        if (modelEmasPerak.getJenis().equals("emas")){
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("jenis",modelEmasPerak.getJenis());
            contentValues.put("tgl_zakat",modelEmasPerak.getTglZakat());
            contentValues.put("jumlah_zakat",modelEmasPerak.getJumlahZakat());
            contentValues.put("harga_pergram",modelEmasPerak.getHargaPergram());
            contentValues.put("flag",sharedPref.getFlagEmas());
            contentValues.put("tgl_hijri",modelEmasPerak.getTglHijri());
            if (flag){
                contentValues.put("flag",1);
                contentValues.put("konversi",modelEmasPerak.getKonversi());
            }else{
                contentValues.put("flag",0);
            }
            db.insertOrThrow("emas","",contentValues);
        }else if (modelEmasPerak.getJenis().equals("perak")){
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("jenis",modelEmasPerak.getJenis());
            contentValues.put("tgl_zakat",modelEmasPerak.getTglZakat());
            contentValues.put("jumlah_zakat",modelEmasPerak.getJumlahZakat());
            contentValues.put("harga_pergram",modelEmasPerak.getHargaPergram());
            contentValues.put("flag",sharedPref.getFlagPerak());
            contentValues.put("tgl_hijri",modelEmasPerak.getTglHijri());
            if (flag){
                contentValues.put("flag",1);
                contentValues.put("konversi",modelEmasPerak.getKonversi());
            }else{
                contentValues.put("flag",0);
            }
            db.insertOrThrow("perak","",contentValues);
        }
    }
    public List<ModelEmasPerak> getZakat(){
        List<ModelEmasPerak> models = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from emas",null);
        if (cursor.getCount()==1){
            cursor.moveToFirst();
            ModelEmasPerak model = new ModelEmasPerak();
            model.setIdZakat(cursor.getInt(cursor.getColumnIndex("id_zakat")));
            model.setFlag(cursor.getInt(cursor.getColumnIndex("flag")));
            model.setJumlahZakat(cursor.getDouble(cursor.getColumnIndex("jumlah_zakat")));
            model.setHargaPergram(cursor.getDouble(cursor.getColumnIndex("harga_pergram")));
            model.setTglZakat(cursor.getString(cursor.getColumnIndex("tgl_zakat")));
            model.setJenis(cursor.getString(cursor.getColumnIndex("jenis")));
            model.setTglHijri(cursor.getString(cursor.getColumnIndex("tgl_hijri")));
            model.setKonversi(cursor.getDouble(cursor.getColumnIndex("konversi")));
            models.add(model);
        }else{
            while (cursor.moveToNext()){
                ModelEmasPerak model = new ModelEmasPerak();
                model.setIdZakat(cursor.getInt(cursor.getColumnIndex("id_zakat")));
                model.setFlag(cursor.getInt(cursor.getColumnIndex("flag")));
                model.setJumlahZakat(cursor.getDouble(cursor.getColumnIndex("jumlah_zakat")));
                model.setHargaPergram(cursor.getDouble(cursor.getColumnIndex("harga_pergram")));
                model.setTglZakat(cursor.getString(cursor.getColumnIndex("tgl_zakat")));
                model.setJenis(cursor.getString(cursor.getColumnIndex("jenis")));
                model.setTglHijri(cursor.getString(cursor.getColumnIndex("tgl_hijri")));
                model.setKonversi(cursor.getDouble(cursor.getColumnIndex("konversi")));
                models.add(model);
            }

        }
        cursor = db.rawQuery("select * from perak",null);
        if (cursor.getCount()==1){
            cursor.moveToFirst();
            ModelEmasPerak model = new ModelEmasPerak();
            model.setIdZakat(cursor.getInt(cursor.getColumnIndex("id_zakat")));
            model.setFlag(cursor.getInt(cursor.getColumnIndex("flag")));
            model.setJumlahZakat(cursor.getDouble(cursor.getColumnIndex("jumlah_zakat")));
            model.setHargaPergram(cursor.getDouble(cursor.getColumnIndex("harga_pergram")));
            model.setTglZakat(cursor.getString(cursor.getColumnIndex("tgl_zakat")));
            model.setJenis(cursor.getString(cursor.getColumnIndex("jenis")));
            model.setTglHijri(cursor.getString(cursor.getColumnIndex("tgl_hijri")));
            model.setKonversi(cursor.getDouble(cursor.getColumnIndex("konversi")));
            models.add(model);
        }else{
            while (cursor.moveToNext()){
                ModelEmasPerak model = new ModelEmasPerak();
                model.setIdZakat(cursor.getInt(cursor.getColumnIndex("id_zakat")));
                model.setFlag(cursor.getInt(cursor.getColumnIndex("flag")));
                model.setJumlahZakat(cursor.getDouble(cursor.getColumnIndex("jumlah_zakat")));
                model.setHargaPergram(cursor.getDouble(cursor.getColumnIndex("harga_pergram")));
                model.setTglZakat(cursor.getString(cursor.getColumnIndex("tgl_zakat")));
                model.setJenis(cursor.getString(cursor.getColumnIndex("jenis")));
                model.setTglHijri(cursor.getString(cursor.getColumnIndex("tgl_hijri")));
                model.setKonversi(cursor.getDouble(cursor.getColumnIndex("konversi")));
                models.add(model);

            }

        }
        cursor.close();
        return models;
    }

    public double getTotalEmas(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select sum(jumlah_zakat) as total from emas where flag = "+ sharedPref.getFlagEmas() +";",null);
        cursor.moveToFirst();
        return cursor.getInt(cursor.getColumnIndex("total"));

    }

    public void pushNisabEmasPerak(ModelNisabEmasPerak modelNisabEmasPerak){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("jenis",modelNisabEmasPerak.getJenis());
        contentValues.put("tanggal_mulaihaul",modelNisabEmasPerak.getTanggal_mulaihaul());
        contentValues.put("tanggal_akhirhaul",modelNisabEmasPerak.getTanggal_akhirhaul());
        contentValues.put("jumlah_harta",modelNisabEmasPerak.getJumlah_harta());
        contentValues.put("jumlah_nisab",modelNisabEmasPerak.getJumlah_nisab());
        contentValues.put("konversi",modelNisabEmasPerak.getKonversi());
        contentValues.put("tanggal_hijriawal",modelNisabEmasPerak.getTanggal_hijriawal());
        contentValues.put("tanggal_hijriahir",modelNisabEmasPerak.getTanggal_hijriahir());
        contentValues.put("id_tabelawal",modelNisabEmasPerak.getId_tabelawal());
        db.insertOrThrow("nisabhaul","",contentValues);
    }
    public int getLastID (String jenis){
        int lastid = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select id_zakat from "+jenis+" order by id_zakat desc limit 1";
        Cursor cursor = db.rawQuery(sql,null);
        if (cursor.getCount()==1){
            cursor.moveToFirst();
            lastid = cursor.getInt(cursor.getColumnIndex("id_zakat"));
        }
        return lastid;
    }

    public boolean isHaul(String jenis){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "";
        if (jenis.trim().equalsIgnoreCase("emas")){
            query = "select * from emas";
        }else {
            query = "select * from perak";
        }
        Cursor cursor = db.rawQuery(query,null);
        if (cursor.getCount()%11 == 0&& cursor.getCount()>=11){
            return  true;

        }else {
            return false;
        }
    }

    public int getCount(String jenis){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "";
        if (jenis.trim().equalsIgnoreCase("emas")){
            query = "select * from emas";
        }else {
            query = "select * from perak";
        }
        Cursor cursor = db.rawQuery(query,null);
        cursor.close();
        return cursor.getCount();

    }
    public void hapusItemEmas(int idZakat) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("DELETE FROM emas WHERE id_zakat='" + idZakat + "'");
        database.close();
    }
    public void hapusItemPerak(int idZakat) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("DELETE FROM perak WHERE id_zakat='" + idZakat + "'");
        database.close();
    }
    public int editItemEmas(int idZakat, Double hargaPergram, Double jumlahZakat,String jenis) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("harga_pergram", hargaPergram);
        contentValues.put("jumlah_zakat", jumlahZakat);
        contentValues.put("jenis", jenis);
        return database.update("emas", contentValues, "id_zakat" + idZakat, null);
    }
    public int editItemPerak(int idZakat, Double hargaPergram, Double jumlahZakat,String jenis) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("harga_pergram", hargaPergram);
        contentValues.put("jumlah_zakat", jumlahZakat);
        contentValues.put("jenis", jenis);
        return database.update("perak", contentValues, "id_zakat" + idZakat, null);
    }


}
