package com.ari.kalkulatorzakat.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ARI on 06/09/2017.
 */
public class SharedPref {
    SharedPreferences sp;
    SharedPreferences.Editor spe;

    public SharedPref(Context context) {
        sp = context.getSharedPreferences("kalkulator_zakat", Context.MODE_PRIVATE);
        spe = sp.edit();
    }
    public int getFlagEmas(){
        return sp.getInt("flagEmas",1);
    }
    public void incFlagEmas(){
        int flagEmas = sp.getInt("flagEmas",1);
        flagEmas ++;
        spe.putInt("flagEmas",flagEmas);
    }
    public int getFlagPerak(){
        return  sp.getInt("flagPerak",1);
    }
    public void incFlagPerak(){
        int flagPerak = sp.getInt("flagPerak",1);
        flagPerak ++;
        spe.putInt("flagPerak",flagPerak);
    }
    public void setAwalHaul(String a){
        spe.putString("AwalHaul","");
        spe.commit();
    }
    public String getAwalHaul(){
        return sp.getString("AwalHaul","");
    }

}
