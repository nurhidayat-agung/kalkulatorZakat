package com.ari.kalkulatorzakat.util;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.ari.kalkulatorzakat.R;
import com.ari.kalkulatorzakat.databinding.CastomDialogBinding;

/**
 * Created by ARI on 14/11/2017.
 */
public class CastemDialog extends Dialog {
    private TextView tv_pesan;


    public CastemDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.castom_dialog);
        tv_pesan = (TextView) findViewById(R.id.tv_dialog);
        this.setCancelable(true);
        this.setCanceledOnTouchOutside(true);
    }
    public void setPesan(String pesan){
        tv_pesan.setText(pesan);
    }
}
