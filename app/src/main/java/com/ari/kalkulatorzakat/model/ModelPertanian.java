package com.ari.kalkulatorzakat.model;

import java.io.Serializable;

/**
 * Created by ARI on 21/09/2017.
 */
public class ModelPertanian implements Serializable {
    private int idTani;
    private double hargaJenis;
    private double jumlahPanen;
    private double hasil1;
    private double konversi4;
    private String jenisPanen;
    private String jenis;
    private String tglZakat;
    private String tglHijri;

    public double getKonversi4() {
        return konversi4;
    }

    public void setKonversi4(double konversi4) {
        this.konversi4 = konversi4;
    }

    public String getJenisPanen() {
        return jenisPanen;
    }

    public void setJenisPanen(String jenisPanen) {
        this.jenisPanen = jenisPanen;
    }

    public double getHasil1() {
        return hasil1;
    }

    public void setHasil1(double hasil1) {
        this.hasil1 = hasil1;
    }

    public double getHargaJenis() {
        return hargaJenis;
    }

    public void setHargaJenis(double hargaJenis) {
        this.hargaJenis = hargaJenis;
    }

    public int getIdTani() {
        return idTani;
    }

    public void setIdTani(int idTani) {
        this.idTani = idTani;
    }

    public double getJumlahPanen() {
        return jumlahPanen;
    }

    public void setJumlahPanen(double jumlahPanen) {
        this.jumlahPanen = jumlahPanen;
    }

    public String getTglZakat() {
        return tglZakat;
    }

    public void setTglZakat(String tglZakat) {
        this.tglZakat = tglZakat;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getTglHijri() {
        return tglHijri;
    }

    public void setTglHijri(String tglHijri) {
        this.tglHijri = tglHijri;
    }
}
