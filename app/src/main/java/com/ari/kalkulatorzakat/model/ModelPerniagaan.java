package com.ari.kalkulatorzakat.model;

import java.io.Serializable;

/**
 * Created by ARI on 21/09/2017.
 */
public class ModelPerniagaan implements Serializable{
    private int idNiaga;
    private int flag;
    private double harga3;
    private double modal;
    private double untung;
    private double hutang;
    private double piutang;
    private double konversi;
    private String jenisNisab;
    private String tglZakat;
    private String tglHijri;

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public double getKonversi() {
        return konversi;
    }

    public void setKonversi(double konversi) {
        this.konversi = konversi;
    }

    public double getHarga3() {
        return harga3;
    }

    public void setHarga3(double harga3) {
        this.harga3 = harga3;
    }

    public double getPiutang() {
        return piutang;
    }

    public void setPiutang(double piutang) {
        this.piutang = piutang;
    }

    public String getJenisNisab() {
        return jenisNisab;
    }

    public void setJenisNisab(String jenisNisab) {
        this.jenisNisab = jenisNisab;
    }

    public int getIdNiaga() {
        return idNiaga;
    }

    public void setIdNiaga(int idNiaga) {
        this.idNiaga = idNiaga;
    }

    public double getModal() {
        return modal;
    }

    public void setModal(double modal) {
        this.modal = modal;
    }

    public double getUntung() {
        return untung;
    }

    public void setUntung(double untung) {
        this.untung = untung;
    }

    public double getHutang() {
        return hutang;
    }

    public void setHutang(double hutang) {
        this.hutang = hutang;
    }

    public String getTglZakat() {
        return tglZakat;
    }

    public void setTglZakat(String tglZakat) {
        this.tglZakat = tglZakat;
    }

    public String getTglHijri() {
        return tglHijri;
    }

    public void setTglHijri(String tglHijri) {
        this.tglHijri = tglHijri;
    }
}
