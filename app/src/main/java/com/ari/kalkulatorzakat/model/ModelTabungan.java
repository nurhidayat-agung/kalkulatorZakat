package com.ari.kalkulatorzakat.model;

/**
 * Created by ARI on 06/10/2017.
 */
public class ModelTabungan {
    private int idTabungan;
    private int flag;
    private double jumlahTabungan;
    private double hargaZakat1;
    private double konversi;
    private String jenisNisab;
    private String tglZakat;
    private String tglHijri;

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public double getKonversi() {
        return konversi;
    }

    public void setKonversi(double konversi) {
        this.konversi = konversi;
    }

    public double getHargaZakat1() {
        return hargaZakat1;
    }

    public void setHargaZakat1(double hargaZakat1) {
        this.hargaZakat1 = hargaZakat1;
    }

    public int getIdTabungan() {
        return idTabungan;
    }

    public void setIdTabungan(int idTabungan) {
        this.idTabungan = idTabungan;
    }

    public double getJumlahTabungan() {
        return jumlahTabungan;
    }

    public void setJumlahTabungan(double jumlahTabungan) {
        this.jumlahTabungan = jumlahTabungan;
    }

    public String getJenisNisab() {
        return jenisNisab;
    }

    public void setJenisNisab(String jenisNisab) {
        this.jenisNisab = jenisNisab;
    }

    public String getTglZakat() {
        return tglZakat;
    }

    public void setTglZakat(String tglZakat) {
        this.tglZakat = tglZakat;
    }

    public String getTglHijri() {
        return tglHijri;
    }

    public void setTglHijri(String tglHijri) {
        this.tglHijri = tglHijri;
    }
}
