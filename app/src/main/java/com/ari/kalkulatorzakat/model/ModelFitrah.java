package com.ari.kalkulatorzakat.model;

import java.io.Serializable;

/**
 * Created by ARI on 06/10/2017.
 */
public class ModelFitrah implements Serializable {
    private int idFitrah;
    private double jumlahOrang;
    private double harga5;
    private double hasil3;
    private String jenisZakat;
    private String tglZakat;
    private String tglHijri;


    public double getHasil3() {
        return hasil3;
    }

    public void setHasil3(double hasil3) {
        this.hasil3 = hasil3;
    }

    public double getHarga5() {
        return harga5;
    }

    public void setHarga5(double harga5) {
        this.harga5 = harga5;
    }

    public int getIdFitrah() {
        return idFitrah;
    }

    public void setIdFitrah(int idFitrah) {
        this.idFitrah = idFitrah;
    }

    public double getJumlahOrang() {
        return jumlahOrang;
    }

    public void setJumlahOrang(double jumlahOrang) {
        this.jumlahOrang = jumlahOrang;
    }

    public void setJumlahOrang(int jumlahOrang) {
        this.jumlahOrang = jumlahOrang;
    }

    public String getJenisZakat() {
        return jenisZakat;
    }

    public void setJenisZakat(String jenisZakat) {
        this.jenisZakat = jenisZakat;
    }

    public String getTglHijri() {
        return tglHijri;
    }

    public void setTglHijri(String tglHijri) {
        this.tglHijri = tglHijri;
    }

    public String getTglZakat() {
        return tglZakat;
    }

    public void setTglZakat(String tglZakat) {
        this.tglZakat = tglZakat;
    }
}
