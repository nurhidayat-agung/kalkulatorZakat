package com.ari.kalkulatorzakat.model;

import java.io.Serializable;

/**
 * Created by ARI on 06/10/2017.
 */
public class ModelTernak implements Serializable {
    private int idZakat;
    private Double jumlahTernak;
    private String hasil;
    private String jenisTernak;
    private String tglZakat;
    private String tglHijri;

    public Double getJumlahTernak() {
        return jumlahTernak;
    }

    public void setJumlahTernak(Double jumlahTernak) {
        this.jumlahTernak = jumlahTernak;
    }

    public String getHasil() {
        return hasil;
    }

    public void setHasil(String hasil) {
        this.hasil = hasil;
    }

    public int getIdZakat() {
        return idZakat;
    }

    public void setIdZakat(int idZakat) {
        this.idZakat = idZakat;
    }

    public String getJenisTernak() {
        return jenisTernak;
    }

    public void setJenisTernak(String jenisTernak) {
        this.jenisTernak = jenisTernak;
    }

    public String getTglZakat() {
        return tglZakat;
    }

    public void setTglZakat(String tglZakat) {
        this.tglZakat = tglZakat;
    }

    public String getTglHijri() {
        return tglHijri;
    }

    public void setTglHijri(String tglHijri) {
        this.tglHijri = tglHijri;
    }
}
