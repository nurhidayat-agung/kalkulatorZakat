package com.ari.kalkulatorzakat.model;

import java.io.Serializable;

/**
 * Created by ARI on 24/08/2017.
 */
public class ModelEmasPerak implements Serializable{
    private int idZakat;
    private int flag;
    private double jumlahZakat;
    private double hargaPergram;
    private double konversi;
    private String tglZakat;
    private String tglHijri;
    private String jenis;

    public double getKonversi() {
        return konversi;
    }

    public void setKonversi(double konversi) {
        this.konversi = konversi;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public double getHargaPergram() {
        return hargaPergram;
    }

    public void setHargaPergram(double hargaPergram) {
        this.hargaPergram = hargaPergram;
    }

    public String getTglHijri() {
        return tglHijri;
    }

    public void setTglHijri(String tglHijri) {
        this.tglHijri = tglHijri;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public int getIdZakat() {
        return idZakat;
    }

    public void setIdZakat(int idZakat) {
        this.idZakat = idZakat;
    }

    public double getJumlahZakat() {
        return jumlahZakat;
    }

    public void setJumlahZakat(double jumlahZakat) {
        this.jumlahZakat = jumlahZakat;
    }

    public String getTglZakat() {
        return tglZakat;
    }

    public void setTglZakat(String tglZakat) {
        this.tglZakat = tglZakat;
    }
}
