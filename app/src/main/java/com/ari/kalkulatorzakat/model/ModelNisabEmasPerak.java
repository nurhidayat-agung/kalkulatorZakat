package com.ari.kalkulatorzakat.model;

import java.io.Serializable;

/**
 * Created by ARI on 07/09/2017.
 */
public class ModelNisabEmasPerak implements Serializable {
    private int id_tabelawal;
    private int id_nisabhaul;
    private String jenis;
    private String tanggal_mulaihaul;
    private String tanggal_akhirhaul;
    private double jumlah_harta;
    private double jumlah_nisab;
    private double konversi;
    private String tanggal_hijriawal;
    private String tanggal_hijriahir;

    public int getId_tabelawal() {
        return id_tabelawal;
    }

    public void setId_tabelawal(int id_tabelawal) {
        this.id_tabelawal = id_tabelawal;
    }

    public double getKonversi() {
        return konversi;
    }

    public void setKonversi(double konversi) {
        this.konversi = konversi;
    }

    public String getTanggal_hijriawal() {
        return tanggal_hijriawal;
    }

    public void setTanggal_hijriawal(String tanggal_hijriawal) {
        this.tanggal_hijriawal = tanggal_hijriawal;
    }

    public String getTanggal_hijriahir() {
        return tanggal_hijriahir;
    }

    public void setTanggal_hijriahir(String tanggal_hijriahir) {
        this.tanggal_hijriahir = tanggal_hijriahir;
    }

    public int getId_nisabhaul() {
        return id_nisabhaul;
    }

    public void setId_nisabhaul(int id_nisabhaul) {
        this.id_nisabhaul = id_nisabhaul;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getTanggal_mulaihaul() {
        return tanggal_mulaihaul;
    }

    public void setTanggal_mulaihaul(String tanggal_mulaihaul) {
        this.tanggal_mulaihaul = tanggal_mulaihaul;
    }

    public double getJumlah_harta() {
        return jumlah_harta;
    }

    public void setJumlah_harta(double jumlah_harta) {
        this.jumlah_harta = jumlah_harta;
    }

    public String getTanggal_akhirhaul() {
        return tanggal_akhirhaul;
    }

    public void setTanggal_akhirhaul(String tanggal_akhirhaul) {
        this.tanggal_akhirhaul = tanggal_akhirhaul;
    }

    public double getJumlah_nisab() {
        return jumlah_nisab;
    }

    public void setJumlah_nisab(double jumlah_nisab) {
        this.jumlah_nisab = jumlah_nisab;
    }
}
