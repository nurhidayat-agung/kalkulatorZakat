package com.ari.kalkulatorzakat.submenu.zakatfitrah;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.ari.kalkulatorzakat.R;
import com.ari.kalkulatorzakat.adapter.DBFitrah;
import com.ari.kalkulatorzakat.adapter.RVFitrah;
import com.ari.kalkulatorzakat.model.ModelFitrah;
import com.ari.kalkulatorzakat.util.DateHijri;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by ARI on 26/08/2017.
 */
public class FragFitrah extends Fragment {
    private TextView tvDateNow;
    private TextView tvHijri;
    private Spinner spnFitrah;
    private TextView tvBeras;
    private TextView tvHarga5;
    private EditText edtFitrah;
    private EditText edtHarga4;
    private Button btnFitrah;
    private EditText edtHasil3;
    private RecyclerView rvFitrah;
    private RecyclerView.LayoutManager layoutManager;
    private RVFitrah adapter;
    private List<ModelFitrah> models = new ArrayList<>();
    private DBFitrah dbAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_fitrah,container,false);
        getActivity().setTitle("Zakat Fitrah");
        tvDateNow = (TextView) view.findViewById(R.id.tv_datenow);
        tvHijri = (TextView) view.findViewById(R.id.tv_datenowhijri);
        spnFitrah = (Spinner) view.findViewById(R.id.spn_fitrah);
        tvBeras = (TextView) view.findViewById(R.id.tv_beras);
        tvHarga5 = (TextView) view.findViewById(R.id.tv_harga5);
        edtFitrah = (EditText) view.findViewById(R.id.edt_fitrah);
        edtHarga4 = (EditText) view.findViewById(R.id.edt_harga4);
        btnFitrah = (Button) view.findViewById(R.id.btn_fitrah);
        edtHasil3 = (EditText) view.findViewById(R.id.edt_hasil3);
        rvFitrah = (RecyclerView) view.findViewById(R.id.rv_fitrah);
        layoutManager = new LinearLayoutManager(getActivity());
        dbAdapter = new DBFitrah(getActivity());
        adapter = new RVFitrah(getActivity(),models);
        rvFitrah.setLayoutManager(layoutManager);
        rvFitrah.setAdapter(adapter);
        loatdata();
        btnFitrah.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if (spnFitrah.getSelectedItem().toString().equals("berat")){
                    hitungberat();
                }else{
                    hitungliter();
                }
                ModelFitrah model = new ModelFitrah();
                model.setJenisZakat(spnFitrah.getSelectedItem().toString());
                model.setJumlahOrang(Double.valueOf(edtFitrah.getText().toString()));
                model.setHarga5(Double.valueOf(edtHarga4.getText().toString()));
                model.setHasil3(Double.valueOf(edtHasil3.getText().toString()));
                model.setTglHijri(getHijriDate());
                model.setTglZakat(getMountNow());
                edtFitrah.setText("");
                edtHarga4.setText("");
                edtHasil3.setText("");
                dbproses(model);

            }
        });


        return  view;
    }

    private void dbproses(ModelFitrah model){
        dbAdapter.pushZakat(model);
        rvFitrah.setLayoutManager(layoutManager);
        rvFitrah.setAdapter(adapter);
        loatdata();
    }
    private void loatdata(){
        models.clear();
        models = dbAdapter.getZakat();
        adapter = new RVFitrah(getActivity(),models);
        rvFitrah.setLayoutManager(layoutManager);
        rvFitrah.setAdapter(adapter);
    }
    private void hitungberat(){
        double a,b,c;
        a = Double.valueOf(edtFitrah.getText().toString())+0.24;
        b = Double.valueOf(edtHarga4.getText().toString());
        c = a*b;
        edtHasil3.setText(c+"");
    }
    private void hitungliter(){
        double a,b,c;
        a = Double.valueOf(edtFitrah.getText().toString())+0.26;
        b = Double.valueOf(edtHarga4.getText().toString());
        c = a*b;
        edtHasil3.setText(c+"");
    }
    public String getHijriDate(){
        DateHijri dateHijri = new DateHijri();
        return dateHijri.writeIslamicDate();
    }
    public String getMountNow(){
        Calendar calendar = Calendar.getInstance();
        Date datenow = calendar.getTime();
        calendar.setTime(datenow);
        String hari = new SimpleDateFormat("dd").format(calendar.getTime());
        String bulan = new SimpleDateFormat("MM").format(calendar.getTime());
        String tahun = new SimpleDateFormat("yyyy").format(calendar.getTime());
        return hari + "/" + bulan + "/" + tahun;
    }


}
