package com.ari.kalkulatorzakat.submenu.zakatpertanian;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ari.kalkulatorzakat.R;
import com.ari.kalkulatorzakat.adapter.DBTani;
import com.ari.kalkulatorzakat.adapter.RvTani;
import com.ari.kalkulatorzakat.model.ModelPertanian;
import com.ari.kalkulatorzakat.util.CastemDialog;
import com.ari.kalkulatorzakat.util.DateHijri;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by ARI on 16/09/2017.
 */
public class FragPertanian extends Fragment {
    private TextView tv_DateNow;
    private TextView tv_Hijri;
    private Spinner spn_Tani;
    private Spinner spn_Tani1;
    private EditText edt_Harga6;
    private EditText edt_Tani;
    private Button btn_Tani;
    private Button editZakat;
    private Button hapusZakat;
    private EditText edt_hasil1;
    private RecyclerView rv_Tani;
    private RecyclerView.LayoutManager layoutManager;
    private RvTani adapter;
    private List<ModelPertanian> models = new ArrayList<>();
    private ModelPertanian modelPertanian = new ModelPertanian();
    private DBTani dbAdabter;
    private CastemDialog dialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.flag_pertanian,container,false);
        getActivity().setTitle("Zakat Pertanian");
        dialog = new CastemDialog(getActivity());
        dbAdabter = new DBTani(getActivity());
        tv_DateNow = (TextView) view.findViewById(R.id.tv_datenow);
        tv_Hijri = (TextView) view.findViewById(R.id.tv_datenowhijri);
        spn_Tani = (Spinner) view.findViewById(R.id.spn_tani);
        spn_Tani1 = (Spinner) view.findViewById(R.id.spn_tani1);
        edt_Harga6 = (EditText) view.findViewById(R.id.tv_harga6);
        edt_Tani = (EditText) view.findViewById(R.id.edt_tani);
        btn_Tani = (Button) view.findViewById(R.id.btn_tani);
        editZakat = (Button) view.findViewById(R.id.edit_zakat);
        hapusZakat = (Button) view.findViewById(R.id.hapus_zakat);
        edt_hasil1 = (EditText) view.findViewById(R.id.edt_hasil1);
        rv_Tani = (RecyclerView) view.findViewById(R.id.rv_pertanian);
        layoutManager = new LinearLayoutManager(getActivity());
        models = dbAdabter.getZakat();
        dbAdabter = new DBTani(getActivity());
        adapter = new RvTani(getActivity(),models);
        rv_Tani.setLayoutManager(layoutManager);
        rv_Tani.setAdapter(adapter);
        loatdata();
        btn_Tani.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spn_Tani.getSelectedItem().toString().trim().equalsIgnoreCase("hujan")){
                    if (modelPertanian.getJumlahPanen()>=653){
                        dbAdabter.pushZakat(modelPertanian);
                    }else{
                        dialog.setPesan("nisab tidak tercukupin");
                    }
                    hitunghujan();
                }else{
                    hitungalat();
                }
                ModelPertanian model = new ModelPertanian();
                model.setJenis(spn_Tani.getSelectedItem().toString());
                model.setJumlahPanen(Double.valueOf(edt_hasil1.getText().toString()));
                model.setTglZakat(getMonthNow());
                model.setTglHijri(getHijriDate());
                dbproses(model);
            }
        });
        return view;
    }

    private void dbproses(ModelPertanian model) {
        dbAdabter.pushZakat(model);
        rv_Tani.setLayoutManager(layoutManager);
        rv_Tani.setAdapter(adapter);
        loatdata();
    }

    private void loatdata() {
        models.clear();
        models = dbAdabter.getZakat();
        adapter = new RvTani(getActivity(),models);
        rv_Tani.setLayoutManager(layoutManager);
        rv_Tani.setAdapter(adapter);
    }

    private void hitunghujan() {
       double a,b,c;
        a = Double.valueOf(edt_Tani.getText().toString())*0.02;
        b = Double.valueOf(edt_Harga6.getText().toString());
        c = a*b;
        edt_hasil1.setText(c+"");
        }

    private void hitungalat() {
        double a,b,c;
        a = Double.valueOf(edt_Tani.getText().toString())*0.1;
        b = Double.valueOf(edt_Harga6.getText().toString());
        c = a*b;
        edt_hasil1.setText(c+"");
    }
    private void hitungpadi(){

    }
    private void hitunggsndum(){

    }
    private void hitungjagung(){

    }
    public String getHijriDate(){
        DateHijri dateHijri = new DateHijri();
        return dateHijri.writeIslamicDate();
    }
    public String getMonthNow(){
        Calendar calendar = Calendar.getInstance();
        Date datenow = calendar.getTime();
        calendar.setTime(datenow);
        String hari = new SimpleDateFormat("dd").format(calendar.getTime());
        String bulan = new SimpleDateFormat("MM").format(calendar.getTime());
        String tahun = new SimpleDateFormat("yyyy").format(calendar.getTime());
        return hari + "/" + bulan + "/" + tahun;
    }
}
