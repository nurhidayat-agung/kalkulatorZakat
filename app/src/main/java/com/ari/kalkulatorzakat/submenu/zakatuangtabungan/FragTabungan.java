package com.ari.kalkulatorzakat.submenu.zakatuangtabungan;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ari.kalkulatorzakat.R;
import com.ari.kalkulatorzakat.adapter.DBTabungan;
import com.ari.kalkulatorzakat.adapter.RVTabungan;
import com.ari.kalkulatorzakat.model.ModelNisabEmasPerak;
import com.ari.kalkulatorzakat.model.ModelTabungan;
import com.ari.kalkulatorzakat.util.DateHijri;
import com.ari.kalkulatorzakat.util.SharedPref;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by ARI on 21/09/2017.
 */
public class FragTabungan extends Fragment{
    private RecyclerView rv_Tabungan;
    private RecyclerView.LayoutManager layoutManager;
    private RVTabungan adapter;
    private TextView tv_DateNow;
    private TextView tv_Hijri;
    private Spinner spn_Tabungan;
    private EditText edt_Tabungan;
    private EditText edt_Harga1;
    private Button btn_Tabungan;
    private List<ModelTabungan> modelTabungans = new ArrayList<>();
    private DBTabungan dbTabungan;
    private ModelTabungan modelTabungan = new ModelTabungan();
    private SharedPref sharedPref;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.flag_tabungan,container,false);
        dbTabungan = new DBTabungan(getActivity());
        rv_Tabungan = (RecyclerView) view.findViewById(R.id.rv_tabungan);
        tv_DateNow = (TextView) view.findViewById(R.id.tv_datenow);
        tv_Hijri = (TextView) view.findViewById(R.id.tv_datenowhijri);
        spn_Tabungan = (Spinner) view.findViewById(R.id.spn_tabungan);
        edt_Tabungan = (EditText) view.findViewById(R.id.edt_tabungan);
        edt_Harga1 = (EditText) view.findViewById(R.id.edt_harga1);
        btn_Tabungan = (Button) view.findViewById(R.id.btn_tabungan);
        sharedPref = new SharedPref(getActivity());
        tv_DateNow.setText(getMonthNow());
        tv_Hijri.setText(getHijriDate());
        layoutManager = new LinearLayoutManager(getActivity());
        modelTabungans = dbTabungan.getZakat();
        adapter = new RVTabungan(getActivity(),modelTabungans);
        rv_Tabungan.setLayoutManager(layoutManager);
        rv_Tabungan.setAdapter(adapter);
        btn_Tabungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelTabungan.setTglZakat(tv_DateNow.getText().toString());
                modelTabungan.setJumlahTabungan(Double.valueOf(edt_Tabungan.getText().toString()));
                modelTabungan.setJenisNisab(spn_Tabungan.getSelectedItem().toString());
                modelTabungan.setTglHijri(tv_Hijri.getText().toString());
                if (dbTabungan.getCount(spn_Tabungan.getSelectedItem().toString())%12 == 0) {
                    if (spn_Tabungan.getSelectedItem().toString().trim().equalsIgnoreCase("emas")) {
                        if (modelTabungan.getJumlahTabungan() >= 85) {
                            dbTabungan.pushZakat(modelTabungan);
                        } else {
                            Toast.makeText(getActivity(), "nisab awal haul tidak tercukupin", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (modelTabungan.getJumlahTabungan() >= 595) {
                            dbTabungan.pushZakat(modelTabungan);
                        } else {
                            Toast.makeText(getActivity(), "nisab awal haul tidak tercukupin", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else if (dbTabungan.isHaul(spn_Tabungan.getSelectedItem().toString())) {
                    if (spn_Tabungan.getSelectedItem().toString().trim().equalsIgnoreCase("emas")) {
                        if (modelTabungan.getJumlahTabungan() >= 85) {
                            ModelNisabEmasPerak model = new ModelNisabEmasPerak();
                            model.setJenis(spn_Tabungan.getSelectedItem().toString());
                            model.setJumlah_harta(dbTabungan.getTotalEmas());
                            double nisab = modelTabungan.getJumlahTabungan() /40;
                            model.setJumlah_nisab(nisab);
                            model.setTanggal_akhirhaul(getNextMonth());
                            model.setTanggal_mulaihaul(getMonthNow());
                            model.setTanggal_hijriahir(getNextHijriDate());
                            model.setTanggal_hijriawal(getHijriDate());
                            dbTabungan.pushNisabEmasPerak(model);
                            Toast.makeText(getActivity(), "jumlah nisab = " + nisab, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), "nisab tidak tercapai pada akhir haul", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (modelTabungan.getJumlahTabungan() >= 595) {
                            ModelNisabEmasPerak model = new ModelNisabEmasPerak();
                            model.setJenis(spn_Tabungan.getSelectedItem().toString());
                            model.setJumlah_harta(dbTabungan.getTotalEmas());
                            double nisab = modelTabungan.getJumlahTabungan()/40;
                            model.setJumlah_nisab(nisab);
                            model.setTanggal_akhirhaul(getNextMonth());
                            model.setTanggal_mulaihaul(getMonthNow());
                            model.setTanggal_hijriawal(getHijriDate());
                            model.setTanggal_hijriahir(getNextHijriDate());
                            dbTabungan.pushNisabEmasPerak(model);
                            Toast.makeText(getActivity(), "Jumlah nisab = " + nisab, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), "nisab tidak tercapai pada akhir haul", Toast.LENGTH_SHORT).show();
                        }
                    }
                    dbTabungan.pushZakat(modelTabungan);
                    Toast.makeText(getActivity(), "test3", Toast.LENGTH_SHORT).show();
                }else{
                    dbTabungan.pushZakat(modelTabungan);
                }
                if (dbTabungan.getCount(spn_Tabungan.getSelectedItem().toString()) == 1) {
                    sharedPref.setAwalHaul(getHijriDate());
                }
                modelTabungans = dbTabungan.getZakat();
                adapter = new RVTabungan(getActivity(), modelTabungans);
                rv_Tabungan.setLayoutManager(layoutManager);
                rv_Tabungan.setAdapter(adapter);
                edt_Tabungan.setText("");
            }
        });
        return view;
    }

    private void cekNisabHaul(String jenis){
        if (jenis.equals("tabungan")){
            if (dbTabungan.isHaul(jenis)){
                if (modelTabungan.getJumlahTabungan()>= 85){
                    ModelNisabEmasPerak model = new ModelNisabEmasPerak();
                    model.setJenis("emas");
                    model.setJumlah_harta(dbTabungan.getTotalEmas());
                    double nisab = dbTabungan.getTotalEmas();
                    model.setJumlah_nisab(nisab);
                    model.setTanggal_akhirhaul(getNextMonth());
                    model.setTanggal_mulaihaul(getMonthNow());
                    model.setTanggal_hijriawal(getHijriDate());
                    model.setTanggal_hijriahir(getNextHijriDate());
                    dbTabungan.pushNisabEmasPerak(model);
                }else{
                    Toast.makeText(getActivity(), "nisab tidak tercapai", Toast.LENGTH_SHORT).show();
                }
            }
        }else if (jenis.equals("perak")){
    }
}
    public String getMonthNow(){
        Calendar calendar = Calendar.getInstance();
        Date datenow = calendar.getTime();
        calendar.setTime(datenow);
        String hari = new SimpleDateFormat("dd").format(calendar.getTime());
        String bulan = new SimpleDateFormat("MM").format(calendar.getTime());
        String tahun = new SimpleDateFormat("yyyy").format(calendar.getTime());
        return hari + "/" + bulan + "/" + tahun;
    }
    public String getNextMonth(){
        Calendar calendar = Calendar.getInstance();
        Date datenow = calendar.getTime();
        calendar.setTime(datenow);
        String hari = new SimpleDateFormat("dd").format(calendar.getTime());
        String bulan = new SimpleDateFormat("MM").format(calendar.getTime());
        String tahun = new SimpleDateFormat("yyyy").format(calendar.getTime());
        int nextTahun = Integer.valueOf(tahun);
        nextTahun ++;
        return hari + "/" + bulan + "/" + nextTahun;
    }
    public String getHijriDate(){
        DateHijri dateHijri = new DateHijri();
        return dateHijri.writeIslamicDate();
    }
    public String getNextHijriDate(){
        DateHijri dateHijri = new DateHijri();
        return dateHijri.nextwriteIsalamicDate();
    }
}






