package com.ari.kalkulatorzakat.submenu.zakatperniagaan;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ari.kalkulatorzakat.R;
import com.ari.kalkulatorzakat.adapter.DBNiaga;
import com.ari.kalkulatorzakat.adapter.RVNiaga;
import com.ari.kalkulatorzakat.model.ModelNisabEmasPerak;
import com.ari.kalkulatorzakat.model.ModelPerniagaan;
import com.ari.kalkulatorzakat.util.DateHijri;
import com.ari.kalkulatorzakat.util.SharedPref;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by ARI on 16/09/2017.
 */
public class FragPerniagaan extends Fragment {
    private RecyclerView rvNiaga;
    private RecyclerView.LayoutManager layoutManager;
    private RVNiaga adapter;
    private TextView tv_DateNow;
    private TextView tv_Hijri;
    private Spinner spnNiaga;
    private EditText edtHarga3;
    private EditText edtModal;
    private EditText edtUntung;
    private EditText edtPiutang;
    private EditText edtHutangpn;
    private EditText tvKonversi;
    private Button btnPerniagaan;
    private List<ModelPerniagaan> modelPerniagaen = new ArrayList<>();
    private DBNiaga dbNiaga;
    private ModelPerniagaan modelPerniagaan = new ModelPerniagaan();
    private SharedPref sharedPref;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_perniagaan,container,false);
        tv_DateNow = (TextView) view.findViewById(R.id.tv_datenow);
        tv_Hijri = (TextView) view.findViewById(R.id.tv_datenowhijri);
        spnNiaga = (Spinner) view.findViewById(R.id.spn_niaga);
        edtHarga3 = (EditText) view.findViewById(R.id.edt_harga3);
        edtModal = (EditText) view.findViewById(R.id.edt_modal);
        edtUntung = (EditText) view.findViewById(R.id.edt_untung);
        edtPiutang = (EditText) view.findViewById(R.id.edt_piutang);
        edtHutangpn = (EditText) view.findViewById(R.id.edt_hutangpn);
        rvNiaga = (RecyclerView) view.findViewById(R.id.rv_perniagaan);
        tvKonversi = (EditText) view.findViewById(R.id.tv_konversi2);
        sharedPref = new SharedPref(getActivity());
        tv_Hijri.setText(getHijriDate());
        tv_DateNow.setText(getMonthNow());
        layoutManager = new LinearLayoutManager(getActivity());
        modelPerniagaen = dbNiaga.getZakat();
        rvNiaga.setLayoutManager(layoutManager);
        rvNiaga.setAdapter(adapter);
        btnPerniagaan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                modelPerniagaan.setTglZakat(tv_DateNow.getText().toString());
                modelPerniagaan.setTglHijri(tv_Hijri.getText().toString());
                modelPerniagaan.setJenisNisab(spnNiaga.getSelectedItem().toString());
                modelPerniagaan.setModal(Double.valueOf(edtModal.getText().toString()));
                modelPerniagaan.setUntung(Double.valueOf(edtUntung.getText().toString()));
                modelPerniagaan.setPiutang(Double.valueOf(edtPiutang.getText().toString()));
                modelPerniagaan.setHutang(Double.valueOf(edtHutangpn.getText().toString()));
                modelPerniagaan.setHarga3(Double.valueOf(edtHarga3.getText().toString()));
                if (dbNiaga.getCount(spnNiaga.getSelectedItem().toString())%12 == 0){
                    if (spnNiaga.getSelectedItem().toString().trim().equalsIgnoreCase("emas")){
                        if (modelPerniagaan.getHarga3() >= 85){
                            dbNiaga.pushZakat(modelPerniagaan);
                        }else{
                            Toast.makeText(getActivity(), "nisab awal haul tidak tercukupin", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        if (modelPerniagaan.getHarga3()>=595){
                            dbNiaga.pushZakat(modelPerniagaan);
                        }else{
                            Toast.makeText(getActivity(), "nisab awal haul tidak tercukupin", Toast.LENGTH_SHORT).show();
                        }
                    }
                }else if (dbNiaga.isHaul(spnNiaga.getSelectedItem().toString())){
                    if (spnNiaga.getSelectedItem().toString().trim().equalsIgnoreCase("emas")){
                        if (modelPerniagaan.getHarga3()>= 85){
                            ModelNisabEmasPerak model = new ModelNisabEmasPerak();
                            model.setJenis("emas");
                            model.setTanggal_akhirhaul(getNextMonth());
                            model.setTanggal_mulaihaul(getMonthNow());
                            model.setTanggal_hijriawal(getHijriDate());
                            model.setTanggal_hijriahir(getNextHijriDate());
                            dbNiaga.pustNisabEmasPerak(model);
                        }else{
                            Toast.makeText(getActivity(), "nisab tidak tercapai", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            }
        });



        return view;
    }


    public String getMonthNow(){
        Calendar calendar = Calendar.getInstance();
        Date datenow = calendar.getTime();
        calendar.setTime(datenow);
        String hari = new SimpleDateFormat("dd").format(calendar.getTime());
        String bulan = new SimpleDateFormat("MM").format(calendar.getTime());
        String tahun = new SimpleDateFormat("yyyy").format(calendar.getTime());
        return hari + "/" + bulan + "/" + tahun;
    }
    public String getNextMonth(){
        Calendar calendar = Calendar.getInstance();
        Date datenow = calendar.getTime();
        calendar.setTime(datenow);
        String hari = new SimpleDateFormat("dd").format(calendar.getTime());
        String bulan = new SimpleDateFormat("MM").format(calendar.getTime());
        String tahun = new SimpleDateFormat("yyyy").format(calendar.getTime());
        int nextTahun = Integer.valueOf(tahun);
        nextTahun ++;
        return hari + "/" + bulan + "/" + nextTahun;
    }
    public String getHijriDate(){
        DateHijri dateHijri = new DateHijri();
        return dateHijri.writeIslamicDate();
    }
    public String getNextHijriDate() {
        DateHijri dateHijri = new DateHijri();
        return dateHijri.nextwriteIsalamicDate();
    }
    public void hitungEmas(){
        double a,b,c,d,e;
        a = Double.valueOf(edtModal.getText().toString());
        b = Double.valueOf(edtUntung.getText().toString());
        c = Double.valueOf(edtPiutang.getText().toString());
        d = Double.valueOf(edtHutangpn.getText().toString())*0.025;
        e = a+b+c-d;
        tvKonversi.setText(e+"");
    }
    public void hitungPerak(){
        double a,b,c,d,e;
        a = Double.valueOf(edtModal.getText().toString());
        b = Double.valueOf(edtUntung.getText().toString());
        c = Double.valueOf(edtPiutang.getText().toString());
        d = Double.valueOf(edtHutangpn.getText().toString())*0.025;
        e = a+b+c-d;
        tvKonversi.setText(e+"");
    }
}
