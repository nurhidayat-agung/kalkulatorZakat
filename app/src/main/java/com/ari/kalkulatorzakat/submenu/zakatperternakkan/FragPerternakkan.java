package com.ari.kalkulatorzakat.submenu.zakatperternakkan;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.ari.kalkulatorzakat.R;
import com.ari.kalkulatorzakat.adapter.DBTernak;
import com.ari.kalkulatorzakat.adapter.RVTernak;
import com.ari.kalkulatorzakat.model.ModelTernak;
import com.ari.kalkulatorzakat.util.DateHijri;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by ARI on 16/09/2017.
 */
public class FragPerternakkan extends Fragment {
    private TextView tvDateNow;
    private TextView tvHijri;
    private Spinner spnTernak;
    private EditText edtTernak;
    private EditText edtHasil;
    private Button btnTernak;
    private RecyclerView rvTernak;
    private RecyclerView.LayoutManager layoutManager;
    private RVTernak adapter;
    private List<ModelTernak> models = new ArrayList<>();
    private DBTernak dbAdabter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.flag_perternakkan,container,false);
        getActivity().setTitle("Zakat Pertarnakkan");
        tvDateNow = (TextView) view.findViewById(R.id.tv_datenow);
        tvHijri = (TextView) view.findViewById(R.id.tv_datenowhijri);
        spnTernak = (Spinner) view.findViewById(R.id.spn_ternak);
        edtTernak = (EditText) view.findViewById(R.id.edt_ternak);
        edtHasil = (EditText) view.findViewById(R.id.edt_hasil);
        btnTernak = (Button) view.findViewById(R.id.btn_ternak);
        rvTernak = (RecyclerView) view.findViewById(R.id.rvternak);
        layoutManager = new LinearLayoutManager(getActivity());
        dbAdabter = new DBTernak(getActivity());
        adapter = new RVTernak(getActivity(),models);
        rvTernak.setLayoutManager(layoutManager);
        rvTernak.setAdapter(adapter);
        loaddata();
        btnTernak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spnTernak.getSelectedItem().toString().equals("Sapi")){
                    hitungsapi();
                }else{
                    hitungkambing();
                }
                ModelTernak model = new ModelTernak();
                model.setJenisTernak(spnTernak.getSelectedItem().toString());
                model.setJumlahTernak(Double.valueOf(edtTernak.getText().toString()));
                model.setHasil(String.valueOf(edtHasil.getText().toString()));
                model.setTglZakat(getMonthNow());
                model.setTglHijri(getHijriDate());
                dbproses(model);

            }
        });

        return view;
    }
    private void  dbproses(ModelTernak model){
        dbAdabter.pushZakat(model);
        rvTernak.setLayoutManager(layoutManager);
        rvTernak.setAdapter(adapter);
        loaddata();
    }
    private void loaddata(){
        models.clear();
        models = dbAdabter.getZakat();
        adapter = new RVTernak(getActivity(),models);
        rvTernak.setLayoutManager(layoutManager);
        rvTernak.setAdapter(adapter);
    }
    private void hitungsapi(){
        super.onStart();
        Double nilai = Double.valueOf(edtTernak.getText().toString());
        if (nilai <=39 && nilai >=30){
            edtHasil.setText("seekor anak sapi");
        }
        if (nilai <=59 && nilai >=40){
            edtHasil.setText("seekor sapi satu tahun");
        }
        if (nilai <=69 && nilai >= 60){
            edtHasil.setText("seekor sapi usia dua tahun");
        }
        if (nilai <=79 && nilai >= 70){
            edtHasil.setText("dua ekor anak sapi");
        }
        if (nilai <=89 && nilai >= 80){
            edtHasil.setText("seekor anak sapi dan sapi dua tahun");
        }
        if (nilai <=99 && nilai >= 90){
            edtHasil.setText("dua ekor sapi dua tahun");
        }
        if (nilai <=109 && nilai >=100){
            edtHasil.setText("tiga ekor anak sapi");
        }
        if (nilai <=119 && nilai >= 110){
            edtHasil.setText("dua ekor anak sapi dan seekor sapi usia dua tahun");
        }else{
            edtHasil.setText("tidak wajib zakat");
        }

    }

    private void hitungkambing(){
        super.onStart();
        Double nilai = Double.valueOf(edtTernak.getText().toString());
        if (nilai <= 120 && nilai >= 40){
            edtHasil.setText("seekor kambing usia dua tahun");
        }
        if (nilai <=200 && nilai >=121){
            edtHasil.setText("dua ekor kambing usia dua tahun");
        }
        if (nilai <=300 && nilai >= 201){
            edtHasil.setText("tiga ekor kambing usia dua tahun");
        }
        if (nilai <=400 && nilai >= 301){
            edtHasil.setText("empat ekor kambing usia dua tahun");
        }
        if (nilai <=500 && nilai >= 401){
            edtHasil.setText("lima ekor kambing usia dua tahun");
        }else{
            edtHasil.setText("tidak wajib zakat");
        }
    }
    public String getHijriDate(){
        DateHijri dateHijri = new DateHijri();
        return dateHijri.writeIslamicDate();
    }
    public String getMonthNow(){
        Calendar calendar = Calendar.getInstance();
        Date datenow = calendar.getTime();
        calendar.setTime(datenow);
        String  hari = new SimpleDateFormat("dd").format(calendar.getTime());
        String bulan = new SimpleDateFormat("MM").format(calendar.getTime());
        String tahun = new SimpleDateFormat("yyyy").format(calendar.getTime());
        return hari + "/" + bulan + "/" + tahun;
    }
}
