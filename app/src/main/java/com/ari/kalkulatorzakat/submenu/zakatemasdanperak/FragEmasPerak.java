package com.ari.kalkulatorzakat.submenu.zakatemasdanperak;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ari.kalkulatorzakat.MainMenu;
import com.ari.kalkulatorzakat.R;
import com.ari.kalkulatorzakat.adapter.DBEmasPerak;
import com.ari.kalkulatorzakat.adapter.RVEmasPerak;
import com.ari.kalkulatorzakat.model.ModelEmasPerak;
import com.ari.kalkulatorzakat.model.ModelNisabEmasPerak;
import com.ari.kalkulatorzakat.util.CastemDialog;
import com.ari.kalkulatorzakat.util.DateHijri;
import com.ari.kalkulatorzakat.util.SharedPref;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by ARI on 22/08/2017.
 */
public class FragEmasPerak extends Fragment{
    private RecyclerView rvEmasPerak;
    private RecyclerView.LayoutManager layoutManager;
    private RVEmasPerak adapter;
    private TextView tvDateNow;
    private TextView tvHijri;
    private Spinner spnEmasperak;
    private EditText edtEmasperak;
    private EditText edtHarga;
    private Button btnEmasperak;
    private List<ModelEmasPerak> modelEmasPeraks = new ArrayList<>();
    private DBEmasPerak dbEmasPerak;
    private ModelEmasPerak modelEmasPerak = new ModelEmasPerak();
    private SharedPref sharedPref;
    private CastemDialog dialog;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_emasperak,container,false);
        getActivity().setTitle("Zakat Emas dan Perak");
        dialog = new CastemDialog(getActivity());
        dbEmasPerak = new DBEmasPerak(getActivity());
        rvEmasPerak = (RecyclerView) view.findViewById(R.id.rv_emasperak);
        tvDateNow = (TextView) view.findViewById(R.id.tv_datenow);
        tvHijri = (TextView) view.findViewById(R.id.tv_datenowhijri);
        spnEmasperak = (Spinner) view.findViewById(R.id.spn_emasperak);
        edtEmasperak = (EditText) view.findViewById(R.id.edt_emasperak);
        edtHarga = (EditText) view.findViewById(R.id.edt_harga);
        btnEmasperak = (Button) view.findViewById(R.id.btn_emasperak);
        sharedPref = new SharedPref(getActivity());
        tvDateNow.setText(getMonthNow());
        tvHijri.setText(getHijriDate());
        layoutManager = new LinearLayoutManager(getActivity());
        modelEmasPeraks = dbEmasPerak.getZakat();
        adapter = new RVEmasPerak(getActivity(),modelEmasPeraks,dbEmasPerak);
        rvEmasPerak.setLayoutManager(layoutManager);
        rvEmasPerak.setAdapter(adapter);
        btnEmasperak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelEmasPerak.setTglZakat(tvDateNow.getText().toString());
                modelEmasPerak.setJumlahZakat(Double.valueOf(edtEmasperak.getText().toString()));
                modelEmasPerak.setJenis(spnEmasperak.getSelectedItem().toString());
                modelEmasPerak.setTglHijri(tvHijri.getText().toString());
                modelEmasPerak.setHargaPergram(Double.valueOf(edtHarga.getText().toString()));
                if (dbEmasPerak.getCount(spnEmasperak.getSelectedItem().toString())%12 == 0){
                    if(spnEmasperak.getSelectedItem().toString().trim().equalsIgnoreCase("emas")){
                        if (modelEmasPerak.getJumlahZakat()>= 85){
                            dbEmasPerak.pushZakat(modelEmasPerak,false);
                        }else{
                            Toast.makeText(getActivity(), "nisab awal haul tidak tercukupin", Toast.LENGTH_SHORT).show();
                            dialog.setPesan("nisab awal haul tidak tercukupin");
                            dialog.show();
                        }
                    }else{
                        if (modelEmasPerak.getJumlahZakat()>= 595){
                            dbEmasPerak.pushZakat(modelEmasPerak,false);
                        }else{
                            Toast.makeText(getActivity(), "nisab awal haul tidak tercukupin", Toast.LENGTH_SHORT).show();
                        }
                    }
                }else if (dbEmasPerak.isHaul(spnEmasperak.getSelectedItem().toString())){
                    if (spnEmasperak.getSelectedItem().toString().trim().equalsIgnoreCase("emas")){
                        if (modelEmasPerak.getJumlahZakat()>= 85){
                            ModelNisabEmasPerak model = new ModelNisabEmasPerak();
                            model.setJenis(spnEmasperak.getSelectedItem().toString());
                            model.setJumlah_harta(dbEmasPerak.getTotalEmas());
                            double nisab = modelEmasPerak.getJumlahZakat()/40;
                            double uang = nisab * Double.valueOf(edtHarga.getText().toString());
                            model.setKonversi(uang);
                            model.setJumlah_nisab(nisab);
                            model.setTanggal_akhirhaul(getNextMonth());
                            model.setTanggal_mulaihaul(getMonthNow());
                            model.setTanggal_hijriawal(getHijriDate());
                            model.setTanggal_hijriahir(getNextHijriDate());
                            int a = dbEmasPerak.getLastID(spnEmasperak.getSelectedItem().toString())+1;
                            model.setId_tabelawal(a);
                            modelEmasPerak.setKonversi(uang);
                            dbEmasPerak.pushNisabEmasPerak(model);
                            Toast.makeText(getActivity(), "jumlah nisab = "+nisab, Toast.LENGTH_SHORT).show();

                        }else{
                            Toast.makeText(getActivity(), "nisab tidak tercapai pada awal atau akhir haul", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        if (modelEmasPerak.getJumlahZakat()>= 595){
                            ModelNisabEmasPerak model = new ModelNisabEmasPerak();
                            model.setJenis(spnEmasperak.getSelectedItem().toString());
                            model.setJumlah_harta(dbEmasPerak.getTotalEmas());
                            double nisab = modelEmasPerak.getJumlahZakat()/40;
                            model.setJumlah_nisab(nisab);
                            model.setTanggal_akhirhaul(getNextMonth());
                            model.setTanggal_mulaihaul(getMonthNow());
                            model.setTanggal_hijriawal(getHijriDate());
                            model.setTanggal_hijriahir(getNextHijriDate());
                            dbEmasPerak.pushNisabEmasPerak(model);
                            Toast.makeText(getActivity(), "jumlah nisab = "+nisab, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), "nisab tidak tercapai pada akhir haul", Toast.LENGTH_SHORT).show();
                        }
                    }
                    dbEmasPerak.pushZakat(modelEmasPerak,true);
                    Toast.makeText(getActivity(), "test3", Toast.LENGTH_SHORT).show();
                }else{
                    dbEmasPerak.pushZakat(modelEmasPerak,false);
                }
                prosesAKhirNisab();
                if (dbEmasPerak.getCount(spnEmasperak.getSelectedItem().toString())== 1){
                    sharedPref.setAwalHaul(getHijriDate());
                }
                modelEmasPeraks = dbEmasPerak.getZakat();
                adapter = new RVEmasPerak(getActivity(),modelEmasPeraks,dbEmasPerak);
                rvEmasPerak.setLayoutManager(layoutManager);
                rvEmasPerak.setAdapter(adapter);
                edtEmasperak.setText("");
                edtHarga.setText("");
            }
        });

        return view;
    }

    private void cekNisabHaul(String jenis) {
        if (jenis.equals("emas")){
            if (dbEmasPerak.isHaul(jenis)){
                if (modelEmasPerak.getJumlahZakat() >= 85){
                    ModelNisabEmasPerak model = new ModelNisabEmasPerak();
                    model.setJenis("emas");
                    model.setJumlah_harta(dbEmasPerak.getTotalEmas());
                    double nisab = dbEmasPerak.getTotalEmas()/40;
                    model.setJumlah_nisab(nisab);
                    model.setTanggal_akhirhaul(getNextMonth());
                    model.setTanggal_mulaihaul(getMonthNow());
                    model.setTanggal_hijriawal(getHijriDate());
                    model.setTanggal_hijriahir(getNextHijriDate());
                    dbEmasPerak.pushNisabEmasPerak(model);

                }else{
                    Toast.makeText(getActivity(), "nisab tidak tercapai", Toast.LENGTH_SHORT).show();
                }

            }


        }else if (jenis.equals("perak")){

        }
    }

    public String getMonthNow(){
        Calendar calendar = Calendar.getInstance();
        Date datenow = calendar.getTime();
        calendar.setTime(datenow);
        String hari = new SimpleDateFormat("dd").format(calendar.getTime());
        String bulan = new SimpleDateFormat("MM").format(calendar.getTime());
        String tahun = new SimpleDateFormat("yyyy").format(calendar.getTime());
        return hari + "/" + bulan + "/" + tahun;
    }
    public String getNextMonth(){
        Calendar calendar = Calendar.getInstance();
        Date datenow = calendar.getTime();
        calendar.setTime(datenow);
        String hari = new SimpleDateFormat("dd").format(calendar.getTime());
        String bulan = new SimpleDateFormat("MM").format(calendar.getTime());
        String tahun = new SimpleDateFormat("yyyy").format(calendar.getTime());
        int nextTahun = Integer.valueOf(tahun);
        nextTahun ++;
        return hari + "/" + bulan + "/" + nextTahun;
    }
    public String getHijriDate(){
        DateHijri dateHijri = new DateHijri();
        return dateHijri.writeIslamicDate();
    }
    public String getNextHijriDate(){
        DateHijri dateHijri = new DateHijri();
        return dateHijri.nextwriteIsalamicDate();
    }
    public void prosesAKhirNisab(){
    }

}
